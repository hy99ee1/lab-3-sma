
enum InputType {
    NETWORK("1"),
    FILE("1");

    private String text;

    InputType(String text) {
        this.text = text;
    }

    public static InputType fromString(String text) {
        for (InputType b : InputType.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
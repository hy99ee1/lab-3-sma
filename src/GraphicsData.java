import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import java.util.ArrayList;
import java.util.List;

public class GraphicsData {
    public PointReceiver pr = null;
    public List<Double> smaX = new ArrayList<>();
    public List<Double> smaY = new ArrayList<>();

    public GraphicsData(PointReceiver pr) {
        this.pr = pr;
    }
}

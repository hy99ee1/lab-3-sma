import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class UrlPointReceiver implements PointReceiver {
    private final String pointSeparator;
    private final String coordinateSeparator;
    private final URL url;
    private final ArrayList<Double> x;
    private final ArrayList<Double> y;

    public UrlPointReceiver(URL url, String pointSeparator, String coordinateSeparator) {
        this.url = url;
        this.pointSeparator = pointSeparator;
        this.coordinateSeparator = coordinateSeparator;
        this.x = new ArrayList<>();
        this.y = new ArrayList<>();
    }


    @Override
    public void Receive() throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(this.url.openStream());
        byte[] buffer = new byte[1024];
        int readied = 0;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while ((readied = bufferedInputStream.read(buffer, 0, 1024)) != -1) {
            byteArrayOutputStream.write(buffer, 0, readied);
        }
        bufferedInputStream.close();
        String points = byteArrayOutputStream.toString();
        byteArrayOutputStream.close();;
        String[] separatedPoints = points.split(pointSeparator);
        Arrays.stream(separatedPoints).forEach(sp -> {
            String[] coordinates = sp.split(coordinateSeparator);
            if (coordinates.length != 2) {
                return;
            }
            x.add(Double.parseDouble(coordinates[0]));
            y.add(Double.parseDouble(coordinates[1]));
        });
    }

    public ArrayList<Double> GetX() {
        return x;
    }

    public ArrayList<Double> GetY() {
        return y;
    }

    @Override
    public void printArrays() {
        System.out.print("x:");
        System.out.println(GetX());

        System.out.print("y:");
        System.out.println(GetY());
    }
}

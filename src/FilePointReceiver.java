import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

public class FilePointReceiver implements PointReceiver {
    private final String pointSeparator;
    private final String coordinateSeparator;
    private final Path path;
    private final ArrayList<Double> x;
    private final ArrayList<Double> y;

    public FilePointReceiver(Path p, String pointSeparator, String coordinateSeparator) {
        this.pointSeparator = pointSeparator;
        this.coordinateSeparator = coordinateSeparator;
        this.path = p;
        this.x = new ArrayList<>();
        this.y = new ArrayList<>();
    }

    @Override
    public void Receive() throws IOException {
        BufferedReader bufferedReader = Files.newBufferedReader(this.path);
        String line = bufferedReader.readLine();
        bufferedReader.close();
        Arrays.stream(line.split(this.pointSeparator)).forEach(point -> {
            String[] coordinates = point.split(this.coordinateSeparator);
            if (coordinates.length != 2) {
                return;
            }
            x.add(Double.parseDouble(coordinates[0]));
            y.add(Double.parseDouble(coordinates[1]));
        });
    }

    public ArrayList<Double> GetX() {
        return x;
    }

    public ArrayList<Double> GetY() {
        return y;
    }

    @Override
    public void printArrays() {
        System.out.print("x:");
        System.out.println(GetX());

        System.out.print("y:");
        System.out.println(GetY());
    }
}

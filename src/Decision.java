import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;



public class Decision {

    private BufferedReader reader = null;

    public Decision() {
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void startDecision()  throws IOException, URISyntaxException {
        PointReceiver pr = buildPointReceiver();
        if (pr == null) {
            throw new UnexpectedException("Input error");
        }

        pr.Receive();
        pr.printArrays();

        System.out.println("Period: ");
        String period = reader.readLine();
        System.out.println(period);

        List<Double> y = pr.GetY();
        double sumY = y.get(0);
        GraphicsData graphicsData = new GraphicsData(pr);
        for (int i = 1; i < y.size(); i++) {
            if (i % Integer.parseInt(period) == 0) {
                graphicsData.smaX.add((double) i);
                graphicsData.smaY.add(sumY / Integer.parseInt(period));
                sumY = y.get(i);
            } else if (i == y.size() - 1) {
                graphicsData.smaX.add((double) i);
                graphicsData.smaY.add(sumY / Integer.parseInt(period));
            } else {
                sumY += y.get(i);
            }

        }

        GraphicsCreator graphicsCreator = new GraphicsCreator(graphicsData);
        graphicsCreator.showGraphics();


    }


    private PointReceiver buildPointReceiver() throws IOException, URISyntaxException {
        PointReceiver result = null;
        boolean isEnd = false;
        InputType type = getInputMethodType();
        while (!isEnd) {
            System.out.println("Enter point separator: ");
            String pointSeparator = reader.readLine();
            System.out.println("Enter coords data separator: ");
            switch (type) {
                case FILE -> {
                    System.out.println("Enter file path: ");
                    String filepath = reader.readLine();
                    String coordinateSeparator = reader.readLine();
                    result = new FilePointReceiver(Paths.get(filepath), pointSeparator, coordinateSeparator);
                    isEnd = true;
                }
                case NETWORK -> {
                    System.out.println("Enter URL: ");
                    String path = reader.readLine();
                    String coordinateSeparator = reader.readLine();
                    URI uri = new URI(path);
                    result = new UrlPointReceiver(uri.toURL(), pointSeparator, coordinateSeparator);
                    isEnd = true;
                }
                default -> {
                    System.out.println("Please follow the instructions ");
                    isEnd = true;
                }
            }
        }
        return result;
    }

    private InputType getInputMethodType() throws IOException {
        System.out.println("\nEnter 1 - From file\nEnter 2 - From network \nAnother one to exit");
        String inputString = reader.readLine();

        InputType inputType = InputType.fromString(inputString);
        if (inputType == null){
            System.exit(0);
        }
        return inputType;
    }
}

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class GraphicsCreator {
    private GraphicsData graphicsData;
    GraphicsCreator(GraphicsData graphicsData) {
        this.graphicsData = graphicsData;
    }

    public void showGraphics() {
        XYChart chart = QuickChart.getChart("Chart ", "x", "y", "y(x)", graphicsData.pr.GetX(), graphicsData.pr.GetY());
        chart.addSeries("SMA", graphicsData.smaX, graphicsData.smaY);
        chart.addSeries("SMA", graphicsData.smaX, graphicsData.smaY);
        new SwingWrapper<>(chart).displayChart();
    }
}


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface PointReceiver {
    void Receive() throws IOException, URISyntaxException;
    List<Double> GetX();
    List<Double> GetY();
    void printArrays();
}
